console.log("Hello World");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function welcomeMessage(){
		let fullName = prompt("Enter your full name: ");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your location: ");
		alert("Thank you!");
	
	console.log("Hello, " + fullName);
	console.log("You are " + age + " years old.");
	console.log("You live in " + location);

	}
    welcomeMessage();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBands(){
		let band1 = "1. The Beatles"
		let band2 = "2. Metallica"
		let band3 = "3. The Eagles"
		let band4 = "4. L 'arc~en~Ciel"
		let band5 = "5. Eraserheads"

		console.log(band1);
		console.log(band2);
		console.log(band3);
		console.log(band4);
		console.log(band5);
	}
	favBands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovie(){
		let movie1 = "1. The Godfather";
		const movieRating1 = "Rotten Tomatoes Rating: 97%";
		let movie2 = "2. The Godfather, Part II";
		const movieRating2 = "Rotten Tomatoes Rating: 96%";
		let movie3 = "3. Shawshank Redemption";
		const movieRating3 = "Rotten Tomatoes Rating: 91%";
		let movie4 = "4. To Kill A Mockingbird";
		const movieRating4 = "Rotten Tomatoes Rating: 93%";
		let movie5 = "5. Psycho";
		const movieRating5 = "Rotten Tomatoes Rating: 96%";

		console.log(movie1);
		console.log(movieRating1);
		console.log(movie2);
		console.log(movieRating2);
		console.log(movie3);
		console.log(movieRating3);
		console.log(movie4);
		console.log(movieRating4);
		console.log(movie5);
		console.log(movieRating5);
	}
	favMovie();






/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
alert("Hi! Please add the names of your friends.");

	function printUsers(){
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}
printUsers();



// console.log(friend1);
// console.log(friend2);
